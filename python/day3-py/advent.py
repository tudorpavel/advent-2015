from operator import add

steps_map = {
    "^": [0, 1],
    ">": [1, 0],
    "v": [0, -1],
    "<": [-1, 0]
}


def convert_step(step_str):
    return steps_map[step_str]


def solve_from_list(steps, houses):
    if steps == []:
        return 0

    cur = [0, 0]

    for step in map(convert_step, steps):
        cur = list(map(add, cur, step))
        houses[str(cur[0]) + ',' + str(cur[1])] = 1

    return len(houses.keys())


def solve1(steps):
    houses = {'0,0': 1}

    return solve_from_list(list(steps), houses)


def solve2(steps):
    houses = {'0,0': 1}

    solve_from_list(list(steps)[1::2], houses)

    return solve_from_list(list(steps)[::2], houses)


if __name__ == "__main__":
    inp = input()

    print("Part1:", solve1(inp))
    print("Part2:", solve2(inp))
