(ns advent)

(defn next-coords [[curx cury]
                   dirchar]
  (case dirchar
    \^ [curx (inc cury)]
    \> [(inc curx) cury]
    \v [curx (dec cury)]
    \< [(dec curx) cury]
    ))

(defn solve1 [directions]
  (let [a (to-array-2d [[1]])]
    (doseq [dirchar directions]
      ))
  )

(defn solve2 [directions]
  "Soon..")

(defn read-input []
  (first (clojure.string/split-lines (slurp *in*))))

(defn -main []
  (let [input (read-input)]
    (println "Part 1:" (solve1 input))
    (println "Part 2:" (solve2 input))))
