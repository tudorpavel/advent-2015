(ns advent-test
  (:require [advent :refer :all]
            [clojure.test :refer :all]))

(deftest present-dimensions-test
  (testing "returns integer dimensions sorted for a given present string"
    (is (= '(2 3 4) (present-dimensions "4x2x3")))
    (is (= '(1 1 10) (present-dimensions "1x10x1")))
    ))

(deftest paper-amount-test
  (testing "returns amount of wrapping paper needed for a given present"
    (is (= 58 (paper-amount '(2 3 4))))
    (is (= 43 (paper-amount '(1 1 10))))
    ))

(deftest ribbon-amount-test
  (testing "returns amount of ribbon needed for a given present"
    (is (= 34 (ribbon-amount '(2 3 4))))
    (is (= 14 (ribbon-amount '(1 1 10))))
    ))
