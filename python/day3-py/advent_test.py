import unittest

from advent import solve1, solve2


class AdventTest(unittest.TestCase):
    def test_solve1(self):
        self.assertEqual(solve1(">"), 2)
        self.assertEqual(solve1("^>v<"), 4)
        self.assertEqual(solve1("^v^v^v^v^v"), 2)

    def test_solve2(self):
        self.assertEqual(solve2("^v"), 3)
        self.assertEqual(solve2("^>v<"), 3)
        self.assertEqual(solve2("^v^v^v^v^v"), 11)


if __name__ == "__main__":
    unittest.main()
