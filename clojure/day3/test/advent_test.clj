(ns advent-test
  (:require [advent :refer :all]
            [clojure.test :refer :all]))

(deftest next-coords-test
  (testing "computes next coords based on direction char"
    (is (= [0 1] (next-coords [0 0] \^)))
    (is (= [1 0] (next-coords [0 0] \>)))
    (is (= [0 -1] (next-coords [0 0] \v)))
    (is (= [-1 0] (next-coords [0 0] \<)))
    ))

(deftest solve1-test
  (testing "counts houses which received presents with given directions string"
    (is (= 2 (solve1 ">")))
    (is (= 4 (solve1 "^>v<")))
    (is (= 2 (solve1 "^v^v^v^v^v")))
    ))
