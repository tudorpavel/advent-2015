(ns advent)

(defn convert-char [c]
  (if (= c \() 1 -1))

(defn solve1 [str]
  (reduce + (map convert-char (char-array str))))

(defn solve2-iter [n current-pos steps]
  (if (= current-pos -1)
    n
    (if (empty? steps)
      nil
      (recur (inc n)
             (+ current-pos (first steps))
             (rest steps)))))

(defn solve2 [str]
  (solve2-iter 0 0 (map convert-char (char-array str))))

(defn read-input []
  (first (clojure.string/split-lines (slurp *in*))))

(defn -main []
  (let [input (read-input)]
    (println "Part 1:" (solve1 input))
    (println "Part 2:" (solve2 input))))
