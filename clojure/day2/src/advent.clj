(ns advent)

(defn present-dimensions [present-str]
  (sort (map read-string (clojure.string/split present-str #"x"))))

(defn paper-amount [present]
  (let [[l w h] present]
    (+ (* 2 l w) (* 2 w h) (* 2 h l) (* l w))))

(defn ribbon-amount [present]
  (let [[l w h] present]
    (+ l l w w (* l w h))))

(defn solve1 [presents]
  (reduce +
          (map (comp paper-amount present-dimensions)
               presents)))

(defn solve2 [presents]
  (reduce +
          (map (comp ribbon-amount present-dimensions)
               presents)))

(defn read-input []
  (clojure.string/split-lines (slurp *in*)))

(defn -main []
  (let [input (read-input)]
    (println "Part 1:" (solve1 input))
    (println "Part 2:" (solve2 input))))
