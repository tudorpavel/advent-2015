(ns advent-test
  (:require [advent :refer :all]
            [clojure.test :refer :all]))

(deftest part1-examples
  (testing "Part 1 examples"
    (is (= 0 (solve1 "(())")))
    (is (= 0 (solve1 "()()")))
    (is (= 3 (solve1 "))(((((")))
    (is (= -1 (solve1 "())")))
    (is (= -1 (solve1 "))(")))
    (is (= -3 (solve1 ")))")))
    (is (= -3 (solve1 ")())())")))
    ))

(deftest part2-examples
  (testing "Part 2 examples"
    (is (= 1 (solve2 ")")))
    (is (= 5 (solve2 "()())")))
    (is (= nil (solve2 "((")))
    ))
