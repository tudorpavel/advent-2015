# Advent of Code 2015

To run a particular day, `cd` into the folder and run:

```
clj -m advent < input.txt
```

To run tests using Spacemacs and CIDER, first open a CIDER session:

```
SPC m s i
```

Then move cursor on a `deftest` and run it with:

```
SPC m t t
```
